<?php
class pluginMttGalleryLB extends Plugin {
	// This hooks is load when the pages are already setted
    function beforeSiteLoad() {
        global $content;

        // Foreach loaded page modified the page's content
        foreach ($content as $key=>$page) {
            // Get the page content
            $pageContent = $page->contentRaw();
            $myGalleryContent = "";

            // get upload paths for the images belonging to the page
            $imageUploadPathAbsolute = PATH_UPLOADS_PAGES . $page->uuid() . "/";
            $imageUploadPathRelative = HTML_PATH_UPLOADS_PAGES . $page->uuid() . "/";
            
            // put all images for the page into an array
            $imageFiles = preg_grep('~\.(jpeg|jpg|png)$~', scandir($imageUploadPathAbsolute));

            // create the gallery preview images
            $myGalleryContent .= "<div>";
            foreach ($imageFiles as $key => $value) {
                $image_path = $imageUploadPathRelative . $value;
                $image_tn_path = $imageUploadPathRelative . "thumbnails/" . $value;
                $myGalleryContent .= "<a class=\"mttgallerylb-image-link\" href=\"$image_path\" data-lightbox=\"gallery\"><img class=\"mttgallerylb-image\" src=\"$image_tn_path\"></a>";
            }
            $myGalleryContent .= "</div>";

            // Search and replace the placeholder string
            $newPageContent = str_replace("{MTT-GALLERY-LB}", $myGalleryContent, $pageContent);

            // Set the new page content
            $page->setField('content', $newPageContent);
        }
    }

    public function siteHead(){

        $html  = '<link href="'.HTML_PATH_PLUGINS.'mtt-gallery-lb/lightbox/css/lightbox.css" rel="stylesheet">';
        $html .= '<link href="'.HTML_PATH_PLUGINS.'mtt-gallery-lb/css/mtt-gallery-lb.css" rel="stylesheet">';
		return $html;

    }

 	public function siteBodyEnd()
 	{

         $html  = PHP_EOL.'<script src="'.HTML_PATH_PLUGINS.'mtt-gallery-lb/lightbox/js/lightbox.js"></script>'.PHP_EOL;
         $html .= "<script>lightbox.option({'disableScrolling': true})</script>";
 		return $html;
	}
}
?>