Last Changed: 2020-02-09
Dependencies: Lightbox (https://lokeshdhakar.com/projects/lightbox2/)
Current included Lightbox Version: 2.11.1

Thanks to Lokesh Dhakar (https://lokeshdhakar.com/) for the lightbox script and some ideas for styling the gallery previews.